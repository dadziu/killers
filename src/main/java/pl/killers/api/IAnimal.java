package pl.killers.api;

public interface IAnimal extends ICreature {
    int SPEED_RATE = 3;
    int STRENGTH_RATE = 1;
    int HIT_POINTS = 100;
}
