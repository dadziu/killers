package pl.killers.api;

public interface IHuman extends ICreature {
    int SPEED_RATE = 2;
    int STRENGTH_RATE = 2;
    int HIT_POINTS = 100;
}
