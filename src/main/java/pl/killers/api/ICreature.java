package pl.killers.api;

public interface ICreature {
    String getName();
    double getHitPoints();
    double getSpeed();
    double getStrength();
}
