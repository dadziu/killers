package pl.killers.statistics;

public enum ECreatureSpeed {

    AXEMAN(1.4),
    CAT(3.4),
    COW(3.4),
    HUMAN(1.7),
    MARUDER(1.9),
    ZOMBIE(1.2);

    private double speed;

    ECreatureSpeed(double speed) {
        this.speed = speed;
    }

    public double getSpeed() {
        return speed;
    }
}
