package pl.killers.statistics;

public enum ECreatureStrength {

    AXEMAN(2.3),
    CAT(1.3),
    COW(1.3),
    HUMAN(3.3),
    MARUDER(4.3),
    ZOMBIE(2.2);

    private double strength;

    ECreatureStrength(double strength) {
        this.strength = strength;
    }

    public double getStrength() {
        return strength;
    }
}
