package pl.killers.statistics;

public enum ECreatureHitPoints {

    AXEMAN(1.1),
    CAT(1.4),
    COW(1.4),
    HUMAN(1.0),
    MARUDER(1.4),
    ZOMBIE(0.7);

    private double hitPoints;

    ECreatureHitPoints(double hitPoints) {
        this.hitPoints = hitPoints;
    }

    public double getHitPoints() {
        return hitPoints;
    }
}
