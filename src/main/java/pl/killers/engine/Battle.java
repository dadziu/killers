package pl.killers.engine;

import pl.killers.api.ICreature;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Battle {

    private ICreature firstWarrior;
    private ICreature secondWarrior;
    private ICreature wonWarrior;
    private double hitPower;
    private double firstWarriorHP;
    private double secondWarriorHP;
    private ICreature winner;
    private static int firstWins;
    private static int secondWins;
    private int numberOfFights;

    public Battle(ICreature firstWarrior, ICreature secondWarrior, int numberOfFights) {

        this.firstWarrior = firstWarrior;
        this.secondWarrior = secondWarrior;
        this.firstWarriorHP = firstWarrior.getHitPoints();
        this.secondWarriorHP = secondWarrior.getHitPoints();
        this.winner = battleLoop();
        this.numberOfFights = numberOfFights;
        numberOfFights();
    }

    public double getFirstWarriorHP() {
        return firstWarriorHP;
    }

    public double getSecondWarriorHP() {
        return secondWarriorHP;
    }

    public ICreature getWinner() {
        return winner;
    }

    /**
     * Method that returns warrior who can hit now
     *
     * @return who can hit now
     */
    private ICreature whoAttack() {
        Random random = new Random();
        int first = random.nextInt(10);
        int second = random.nextInt(10);

        if(this.firstWarrior.getSpeed() * first > this.secondWarrior.getSpeed() * second) {
            System.out.println("whoAttack() " + firstWarrior.getName() + " --->");
            return this.firstWarrior;
        } else if (this.firstWarrior.getSpeed() * first < this.secondWarrior.getSpeed() * second){
            System.out.println("<--- " + secondWarrior.getName() + " whoAttack()");
            return this.secondWarrior;
        } else {
            System.out.println("||-whoAttack() " + "draw-||");

//            try {
//                TimeUnit.SECONDS.sleep(4);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            return whoAttack();
        }
    }


    /**
     * Method that set how strong Warrior hit
     */
    private void hitPower() {
        Random random = new Random();
        double hitAccuracy = random.nextDouble()*10;

        this.hitPower = wonWarrior.getStrength() * hitAccuracy;
        System.out.printf("%nhitPower() " + "%.2f%n",this.hitPower);
        System.out.println();
    }


    /**
     * Method that set HP of warrior after hit
     */
    private void hpAfterHit() {
        if(this.wonWarrior == this.firstWarrior) {
            this.secondWarriorHP = secondWarriorHP - hitPower;
            System.out.printf("hpAfterHit() " + "%.2f%n",secondWarriorHP);
            System.out.printf("^===-----------------===^%n%n");
        } else {
            this.firstWarriorHP = firstWarriorHP - hitPower;
            System.out.printf("hpAfterHit() " + "%.2f%n",firstWarriorHP);
            System.out.printf("^===-----------------===^%n%n");
        }
    }


    /**
     * Method that returns full battle winner
     *
     * @return full battle winner
     */
    private ICreature battleLoop() {
        while (this.firstWarriorHP > 0) {
            while (this.secondWarriorHP > 0) {

                System.out.printf("==========================%n");
                this.wonWarrior = whoAttack();
                hitPower();

//                try {
//                    TimeUnit.SECONDS.sleep(2);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

                if (firstWarrior == wonWarrior) {
                    System.out.println("v===-----------------===v");
                    System.out.print(this.secondWarrior.getName() + " - ");
                } else if (secondWarrior == wonWarrior) {
                    System.out.println("v===-----------------===v");
                    System.out.print(this.firstWarrior.getName() + " - ");
                } else {
                    whoAttack();
                }
                hpAfterHit();

//                try {
//                    TimeUnit.SECONDS.sleep(4);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

                battleLoop();

                break;
            }
            break;
        }
        if (this.firstWarriorHP > this.secondWarriorHP) {
            return this.firstWarrior;
        } else {
            return this.secondWarrior;
        }
    }

    /**
     * Method that counts wins
     */
    private void winsCounter() {
        if (winner == firstWarrior) {
            this.firstWins++;
        } else {
            this.secondWins++;
        }
    }

    /**
     * Method that shows number of wins for each
     */
    public void howManyTimesWon() {
        System.out.println(this.firstWarrior.getName() + " wins: "  + this.firstWins);
        System.out.println(this.secondWarrior.getName() + " wins: "  + this.secondWins);
    }

    /**
     * Method that repeats this constructor
     */
    public void numberOfFights() {
        winsCounter();
        if(numberOfFights > 1) {
            new Battle(firstWarrior, secondWarrior, numberOfFights-1);
        } else {
            howManyTimesWon();
        }
    }

}