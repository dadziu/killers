package pl.killers.app;

import pl.killers.api.ICreature;
import pl.killers.engine.Battle;
import pl.killers.implementations.Cat;
import pl.killers.implementations.Cow;

public class Killers {
    public static void main (String[] args) {
        ICreature cow = new Cow("COW");
        ICreature cat = new Cat("CAT");

        Battle battle = new Battle(cow, cat, 3);

        System.out.println("===============================================");
        System.out.printf("Left HP - " + cow.getName() + " " + "%.2f%n",battle.getFirstWarriorHP());
        System.out.printf("Left HP - " + cat.getName() + " " + "%.2f%n",battle.getSecondWarriorHP());
        System.out.println("===============================================");
        System.out.println("WINNER: " + battle.getWinner().getName());
        battle.howManyTimesWon();

    }
}
 