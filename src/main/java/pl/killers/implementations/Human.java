package pl.killers.implementations;

import pl.killers.api.IHuman;
import pl.killers.statistics.ECreatureHitPoints;
import pl.killers.statistics.ECreatureSpeed;
import pl.killers.statistics.ECreatureStrength;

public class Human implements IHuman {

    private String name;


    public Human (String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public double getHitPoints() {
        return ECreatureHitPoints.HUMAN.getHitPoints() * HIT_POINTS;
    }

    public double getSpeed() {
        return ECreatureSpeed.HUMAN.getSpeed() * SPEED_RATE;
    }

    public double getStrength() {
        return ECreatureStrength.HUMAN.getStrength() * STRENGTH_RATE;
    }
}
