package pl.killers.implementations;

import pl.killers.api.IHuman;
import pl.killers.statistics.ECreatureHitPoints;
import pl.killers.statistics.ECreatureSpeed;
import pl.killers.statistics.ECreatureStrength;

public class Axeman implements IHuman {

    private String name;


    public Axeman (String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public double getHitPoints() {
        return ECreatureHitPoints.AXEMAN.getHitPoints() * HIT_POINTS;
    }

    public double getSpeed() {
        return ECreatureSpeed.AXEMAN.getSpeed() * SPEED_RATE;
    }

    public double getStrength() {
        return ECreatureStrength.AXEMAN.getStrength() * STRENGTH_RATE;
    }
}
