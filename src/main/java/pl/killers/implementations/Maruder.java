package pl.killers.implementations;

import pl.killers.api.IMonster;
import pl.killers.statistics.ECreatureHitPoints;
import pl.killers.statistics.ECreatureSpeed;
import pl.killers.statistics.ECreatureStrength;

public class Maruder implements IMonster {

    private String name;


    public Maruder (String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public double getHitPoints() {
        return ECreatureHitPoints.MARUDER.getHitPoints() * HIT_POINTS;
    }

    public double getSpeed() {
        return ECreatureSpeed.MARUDER.getSpeed() * SPEED_RATE;
    }

    public double getStrength() {
        return ECreatureStrength.MARUDER.getStrength() * STRENGTH_RATE;
    }
}
