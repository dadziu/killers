package pl.killers.implementations;

import pl.killers.api.IAnimal;
import pl.killers.statistics.ECreatureHitPoints;
import pl.killers.statistics.ECreatureSpeed;
import pl.killers.statistics.ECreatureStrength;

public class Cat implements IAnimal {

    private String name;


    public Cat (String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public double getHitPoints() {
        return ECreatureHitPoints.CAT.getHitPoints() * HIT_POINTS;
    }

    public double getSpeed() {
        return ECreatureSpeed.CAT.getSpeed() * SPEED_RATE;
    }

    public double getStrength() {
        return ECreatureStrength.CAT.getStrength() * STRENGTH_RATE;
    }
}
