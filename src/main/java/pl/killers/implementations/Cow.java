package pl.killers.implementations;

import pl.killers.api.IAnimal;
import pl.killers.statistics.ECreatureHitPoints;
import pl.killers.statistics.ECreatureSpeed;
import pl.killers.statistics.ECreatureStrength;

public class Cow implements IAnimal {

    private String name;


    public Cow (String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public double getHitPoints() {
        return ECreatureHitPoints.COW.getHitPoints() * HIT_POINTS;
    }

    public double getSpeed() {
        return ECreatureSpeed.COW.getSpeed() * SPEED_RATE;
    }

    public double getStrength() {
        return ECreatureStrength.COW.getStrength() * STRENGTH_RATE;
    }
}
