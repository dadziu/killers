package pl.killers.implementations;

import pl.killers.api.IMonster;
import pl.killers.statistics.ECreatureHitPoints;
import pl.killers.statistics.ECreatureSpeed;
import pl.killers.statistics.ECreatureStrength;

public class Zombie implements IMonster {

    private String name;


    public Zombie (String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public double getHitPoints() {
        return ECreatureHitPoints.ZOMBIE.getHitPoints() * HIT_POINTS;
    }

    public double getSpeed() {
        return ECreatureSpeed.ZOMBIE.getSpeed() * SPEED_RATE;
    }

    public double getStrength() {
        return ECreatureStrength.ZOMBIE.getStrength() * STRENGTH_RATE;
    }
}
